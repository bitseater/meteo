# METEO
# Copyright (C) 2017 Carlos Suárez <bitseater@gmail.com>.
# This file is distributed under the same license as the com.gitlab.bitseater.meteo package.
#
msgid ""
msgstr ""
"Project-Id-Version: Weather\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-30 13:21+0200\n"
"PO-Revision-Date: 2024-01-30 21:13+0100\n"
"Last-Translator: Andrey Kultyapov <camellan@yandex.ru>\n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 3.4.2\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: src/Widgets/Preferences.vala:146
msgid "1 hr."
msgstr "1 ч."

#: src/Widgets/Preferences.vala:149
msgid "12 hrs."
msgstr "12 ч."

#: src/Widgets/Preferences.vala:147
msgid "2 hrs."
msgstr "2 ч."

#: src/Widgets/Preferences.vala:150
msgid "24 hrs."
msgstr "24 ч."

#: src/Widgets/Preferences.vala:148
msgid "6 hrs."
msgstr "6 ч."

#: src/Widgets/About.vala:30
msgid "A forecast application with OpenWeatherMap API"
msgstr "Приложение для прогноза погоды использующее API OpenWeatherMap"

#: src/Utils/OWM_Today.vala:300
msgid "API key"
msgstr "Ключ API"

#: src/Widgets/Header.vala:36
msgid "About Meteo"
msgstr "О Meteo"

#: src/Widgets/City.vala:85
msgid "At least of 3 characters are required!"
msgstr "Требуется не менее 3 символов!"

#: src/Widgets/About.vala:57
msgid "Based in a mockup by"
msgstr "Основано на макете от"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:43
msgid "Carlos Suarez"
msgstr "Carlos Suarez"

#: src/Widgets/Header.vala:57
msgid "Change location"
msgstr "Выбрать местоположение"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:22
msgid "Choose your city with popup maps"
msgstr "Выберите свой город с информацией на карте"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:21
msgid "Choose your units from metric, imperial or british systems"
msgstr "Выберите единицы (метрические, имперские или британские)"

#: src/Widgets/Preferences.vala:272
msgid "Close"
msgstr "Закрыть"

#: src/Widgets/Today.vala:70
msgid "Cloudiness"
msgstr "Облачность"

#: src/Utils/OWM_Today.vala:316 src/Widgets/MapView.vala:99
msgid "Clouds"
msgstr "Облачность"

#: src/Widgets/About.vala:55
msgid "Collaborators"
msgstr "Соавторы"

#: src/Utils/OWM_Today.vala:304
msgid "Coord. lat"
msgstr "Широта"

#: src/Utils/OWM_Today.vala:303
msgid "Coord. lon"
msgstr "Долгота"

#: src/Widgets/Today.vala:39
msgid "Coordinates"
msgstr "Координаты"

#: src/Utils/OWM_Today.vala:307 src/Widgets/City.vala:63
msgid "Country"
msgstr "Страна"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:12
msgid ""
"Current weather, with information about temperature, pressure, wind speed "
"and wind direction, sunrise and sunset. Know meteorologic predictions to "
"next hours and days. Show awesome maps with all the information. Switch "
"between some maps distributors. Now with System Tray Indicator, showing your "
"location and forecast."
msgstr ""
"Текущая погода, с информацией о температуре, давлении, скорости ветра и "
"ветре, восходе и закате. Знать метеорологические прогнозы в следующие часы и "
"дни. Покажите удивительные карты со всей информацией. Переключитесь между "
"некоторыми дистрибьюторами карт. Теперь с индикатором системного лотка, "
"показывающим ваше местоположение и прогноз."

#: src/Widgets/Preferences.vala:41
msgid "Dark theme"
msgstr "Тёмная тема"

#: src/Widgets/Header.vala:76
msgid "Data"
msgstr "Прогноз"

#: src/Utils/OWM_Today.vala:309
msgid "Description"
msgstr "Описание"

#: src/Utils/OWM_Today.vala:283
msgid "Error"
msgstr "Ошибка"

#: src/Widgets/Preferences.vala:222
msgid "Find my location automatically"
msgstr "Автоопределение местоположения"

#: src/Widgets/Forecast.vala:93
msgid "Forecast"
msgstr "Прогноз погоды"

#: data/com.gitlab.bitseater.meteo.desktop.in:5
msgid "Forecast App for desktop"
msgstr "Прогноз погоды для рабочего стола"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:19
msgid "Forecast for next 18 hours"
msgstr "Прогноз на следующие 18 часов"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:20
msgid "Forecast for next five days"
msgstr "Прогноз на ближайшие пять дней"

#: src/Utils/GeoLocate.vala:67 src/Widgets/MapView.vala:78
msgid "Found an error"
msgstr "Обнаружена ошибка"

#: src/Utils/OWM_Today.vala:267
msgid "Full Moon"
msgstr "Полнолуние"

#: src/Widgets/Preferences.vala:36
msgid "General"
msgstr "Общие"

#: src/Utils/OWM_Today.vala:313 src/Widgets/Today.vala:58
msgid "Humidity"
msgstr "Влажность"

#: src/Utils/OWM_Today.vala:302
msgid "ID place"
msgstr "ID местоположения"

#: src/Utils/OWM_Today.vala:310
msgid "Icon file"
msgstr "Значок"

#: src/Widgets/Preferences.vala:202
msgid "Imperial System"
msgstr "Имперская система"

#: src/Widgets/Preferences.vala:33
msgid "Interface"
msgstr "Интерфейс"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:10
msgid "Know the forecast of the next hours and days with data and maps"
msgstr ""
"Узнайте прогноз погоды на ближайшие часы и дни с отображением данных на карте"

#: src/Utils/OWM_Today.vala:299
msgid "Language"
msgstr "Язык"

#: src/Utils/OWM_Today.vala:275
msgid "Last Quarter Moon"
msgstr "Убывающая луна"

#: src/Widgets/City.vala:61
msgid "Lat"
msgstr "Широта"

#: src/Widgets/Preferences.vala:123
msgid "Launch on start"
msgstr "Запускать при старте"

#: src/MainWindow.vala:74
msgid "Loading"
msgstr "Загрузкa"

#: src/Utils/OWM_Today.vala:175
msgid "Loading cache data"
msgstr "Загружаем данные из кэша"

#: src/Utils/OWM_Today.vala:305 src/Widgets/City.vala:65
msgid "Location"
msgstr "Местоположение"

#: src/Widgets/City.vala:62
msgid "Lon"
msgstr "Долгота"

#: src/Widgets/Header.vala:77
msgid "Maps"
msgstr "Карта"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:8
#: data/com.gitlab.bitseater.meteo.desktop.in:3
#: data/com.gitlab.bitseater.meteo.desktop.in:4
msgid "Meteo"
msgstr "Meteo"

#: data/com.gitlab.bitseater.meteo.desktop.in:13
msgid "Meteo;Weather;Forecast;Temperature;Wind;Snow;Rain;"
msgstr "Meteo;Погода;прогноз;температура;ветер;снег;дождь;"

#: src/Widgets/Preferences.vala:201
msgid "Metric System"
msgstr "Метрическая система"

#: src/Widgets/Today.vala:80
msgid "Moonphase"
msgstr "Фаза луны"

#: src/Utils/OWM_Today.vala:251
msgid "New Moon"
msgstr "Новолуние"

#: src/Widgets/City.vala:131
msgid "No data"
msgstr "Нет данных"

#: src/Utils/OWM_Today.vala:183
msgid "No data received, loading cache. Are you connected?"
msgstr "Не удается получить данные, проверьте сеть. Загружаем данные из кэша"

#: src/Widgets/Header.vala:51
msgid "Options"
msgstr "Опции"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:17
msgid "Other features:"
msgstr "Другие характеристики:"

#: src/Widgets/MapView.vala:100
msgid "Precipitation"
msgstr "Осадки"

#: src/Widgets/Preferences.vala:24 src/Widgets/Header.vala:35
msgid "Preferences"
msgstr "Настройки"

#: src/Utils/OWM_Today.vala:312 src/Widgets/MapView.vala:101
#: src/Widgets/Today.vala:54
msgid "Pressure"
msgstr "Давление"

#: src/Utils/OWM_Today.vala:259
msgid "Quarter Moon"
msgstr "Растущая луна"

#: src/MainWindow.vala:189
msgid "Quit"
msgstr "Выйти"

#: src/Widgets/City.vala:49
msgid "Search for new location"
msgstr "Поиск нового местоположения"

#: src/MainWindow.vala:180
msgid "Show Meteo"
msgstr "Показать Meteo"

#: src/Widgets/About.vala:58
msgid "Some icons by"
msgstr "Некоторые иконки от"

#: src/Widgets/About.vala:56
msgid "Special Thanks to"
msgstr "Особая благодарность"

#: src/Widgets/Preferences.vala:79
msgid "Start minimized"
msgstr "Запускать в свернутом виде"

#: src/Utils/OWM_Today.vala:306 src/Widgets/City.vala:64
msgid "State"
msgstr "Область"

#: src/Utils/OWM_Today.vala:317 src/Widgets/Today.vala:72
msgid "Sunrise"
msgstr "Восход"

#: src/Utils/OWM_Today.vala:318 src/Widgets/Today.vala:76
msgid "Sunset"
msgstr "Закат"

#: src/Widgets/Preferences.vala:61
msgid "Symbolic icons"
msgstr "Символические значки"

#: src/Utils/OWM_Today.vala:311 src/Widgets/MapView.vala:98
msgid "Temperature"
msgstr "Температура"

#: src/Widgets/MapView.vala:106
msgid "Terms of Service"
msgstr "Условия обслуживания"

#: src/Widgets/Today.vala:36
msgid "Today"
msgstr "Cегодня"

#: src/Widgets/Preferences.vala:203
msgid "UK System"
msgstr "Английская система"

#: src/Utils/OWM_Today.vala:301 src/Widgets/Preferences.vala:198
msgid "Units"
msgstr "Единицы"

#: src/Widgets/Header.vala:59
msgid "Update conditions"
msgstr "Обновить информацию"

#: src/Widgets/Preferences.vala:143
msgid "Update conditions every"
msgstr "Обновлять каждые"

#: src/Widgets/Preferences.vala:97
msgid "Use System Tray Indicator"
msgstr "Отображать индикатор в системном трее"

#: src/Utils/OWM_Today.vala:279
msgid "Waning Crescent Moon"
msgstr "Убывающая луна"

#: src/Utils/OWM_Today.vala:271
msgid "Waning Gibbous Moon"
msgstr "Убывающая луна"

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:23
msgid "Watch awesome maps with weather information"
msgstr "Посмотрите классные карты с информацией о погоде"

#: src/Utils/OWM_Today.vala:255
msgid "Waxing Crescent Moon"
msgstr "Растущая луна"

#: src/Utils/OWM_Today.vala:263
msgid "Waxing Gibbous Moon"
msgstr "Растущая луна"

#: src/Utils/OWM_Today.vala:308
msgid "Weather"
msgstr "Погода"

#: src/Widgets/MapView.vala:102
msgid "Wind Speed"
msgstr "Скорость ветра"

#: src/Utils/OWM_Today.vala:315 src/Widgets/Today.vala:66
msgid "Wind dir"
msgstr "Ветер"

#: src/Utils/OWM_Today.vala:314 src/Widgets/Today.vala:62
msgid "Wind speed"
msgstr "Скорость ветра"

#: src/Widgets/About.vala:49
msgid "website"
msgstr "вебсайт"

#~ msgid "com.gitlab.bitseater.meteo"
#~ msgstr "com.gitlab.bitseater.meteo"

#~ msgid "Country: "
#~ msgstr "Страна: "

#~ msgid "Location: "
#~ msgstr "Местоположение: "

#~ msgid "State: "
#~ msgstr "Область: "

#~ msgid "Change to "
#~ msgstr "Изменение "

#~ msgid "Current Weather Data"
#~ msgstr "Данные о текущей погоде"

#~ msgid "Data file not found"
#~ msgstr "Не найден файл данных"

#~ msgid "Image not found"
#~ msgstr "Изображение не найдено"

#~ msgid "Select"
#~ msgstr "Выбрать"

#~ msgid "View weather data"
#~ msgstr "Просмотр данных о погоде"

#~ msgid "Enter your OpenWeatherMap API key"
#~ msgstr "Введите ключ API OpenWeatherMap"

#~ msgid "If you don't have it, please visit:"
#~ msgstr "Если вы не имеете его, пожалуйста посетите:"

#~ msgid "Now with System Tray Indicator."
#~ msgstr "Теперь с индикатором в системном трее."

#~ msgid "Restore API key:"
#~ msgstr "Восстановить ключ API:"

#~ msgid "Save"
#~ msgstr "Сохранить"

#~ msgid "The API key cannot be empty."
#~ msgstr "Ключ API не может быть пустым."

#~ msgid "Wrong API Key. Please, try again."
#~ msgstr "Неверный ключ API. Пожалуйста, попробуйте еще раз."

#~ msgid "- Use only to try this application"
#~ msgstr "- Использовать только для ознакомления"

#~ msgid "- You can't use the update button"
#~ msgstr "- Вы не можете использовать кнопку обновления"

#~ msgid "- You'll have limited access to API"
#~ msgstr "- Вы будете иметь ограниченный доступ к API"

#~ msgid "Use API key built-in"
#~ msgstr "Использовать встроенный ключ API"

#~ msgid "Using built-in API key:"
#~ msgstr "Используется встроенный API-ключ:"

#~ msgid "or just :"
#~ msgstr "или просто :"

#~ msgid "Clouds: "
#~ msgstr "Облачность: "

#~ msgid "Humidity: "
#~ msgstr "Влажность: "

#~ msgid "Pressure :"
#~ msgstr "Давление :"

#~ msgid "Pressure: "
#~ msgstr "Давление: "

#~ msgid "Sunrise: "
#~ msgstr "Восход: "

#~ msgid "Sunset: "
#~ msgstr "Закат: "

#~ msgid "Temperature: "
#~ msgstr "Температура: "

#~ msgid "Units: "
#~ msgstr "Единицы: "

#~ msgid "Wind dir: "
#~ msgstr "Ветер: "

#~ msgid "Wind speed: "
#~ msgstr "Скорость ветра: "
