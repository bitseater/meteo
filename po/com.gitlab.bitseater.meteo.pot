# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-10-30 13:21+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/Widgets/Preferences.vala:146
msgid "1 hr."
msgstr ""

#: src/Widgets/Preferences.vala:149
msgid "12 hrs."
msgstr ""

#: src/Widgets/Preferences.vala:147
msgid "2 hrs."
msgstr ""

#: src/Widgets/Preferences.vala:150
msgid "24 hrs."
msgstr ""

#: src/Widgets/Preferences.vala:148
msgid "6 hrs."
msgstr ""

#: src/Widgets/About.vala:30
msgid "A forecast application with OpenWeatherMap API"
msgstr ""

#: src/Utils/OWM_Today.vala:300
msgid "API key"
msgstr ""

#: src/Widgets/Header.vala:36
msgid "About Meteo"
msgstr ""

#: src/Widgets/City.vala:85
msgid "At least of 3 characters are required!"
msgstr ""

#: src/Widgets/About.vala:57
msgid "Based in a mockup by"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:43
msgid "Carlos Suarez"
msgstr ""

#: src/Widgets/Header.vala:57
msgid "Change location"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:22
msgid "Choose your city with popup maps"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:21
msgid "Choose your units from metric, imperial or british systems"
msgstr ""

#: src/Widgets/Preferences.vala:272
msgid "Close"
msgstr ""

#: src/Widgets/Today.vala:70
msgid "Cloudiness"
msgstr ""

#: src/Utils/OWM_Today.vala:316 src/Widgets/MapView.vala:99
msgid "Clouds"
msgstr ""

#: src/Widgets/About.vala:55
msgid "Collaborators"
msgstr ""

#: src/Utils/OWM_Today.vala:304
msgid "Coord. lat"
msgstr ""

#: src/Utils/OWM_Today.vala:303
msgid "Coord. lon"
msgstr ""

#: src/Widgets/Today.vala:39
msgid "Coordinates"
msgstr ""

#: src/Utils/OWM_Today.vala:307 src/Widgets/City.vala:63
msgid "Country"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:12
msgid ""
"Current weather, with information about temperature, pressure, wind speed "
"and wind direction, sunrise and sunset. Know meteorologic predictions to "
"next hours and days. Show awesome maps with all the information. Switch "
"between some maps distributors. Now with System Tray Indicator, showing your "
"location and forecast."
msgstr ""

#: src/Widgets/Preferences.vala:41
msgid "Dark theme"
msgstr ""

#: src/Widgets/Header.vala:76
msgid "Data"
msgstr ""

#: src/Utils/OWM_Today.vala:309
msgid "Description"
msgstr ""

#: src/Utils/OWM_Today.vala:283
msgid "Error"
msgstr ""

#: src/Widgets/Preferences.vala:222
msgid "Find my location automatically"
msgstr ""

#: src/Widgets/Forecast.vala:93
msgid "Forecast"
msgstr ""

#: data/com.gitlab.bitseater.meteo.desktop.in:5
msgid "Forecast App for desktop"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:19
msgid "Forecast for next 18 hours"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:20
msgid "Forecast for next five days"
msgstr ""

#: src/Utils/GeoLocate.vala:67 src/Widgets/MapView.vala:78
msgid "Found an error"
msgstr ""

#: src/Utils/OWM_Today.vala:267
msgid "Full Moon"
msgstr ""

#: src/Widgets/Preferences.vala:36
msgid "General"
msgstr ""

#: src/Utils/OWM_Today.vala:313 src/Widgets/Today.vala:58
msgid "Humidity"
msgstr ""

#: src/Utils/OWM_Today.vala:302
msgid "ID place"
msgstr ""

#: src/Utils/OWM_Today.vala:310
msgid "Icon file"
msgstr ""

#: src/Widgets/Preferences.vala:202
msgid "Imperial System"
msgstr ""

#: src/Widgets/Preferences.vala:33
msgid "Interface"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:10
msgid "Know the forecast of the next hours and days with data and maps"
msgstr ""

#: src/Utils/OWM_Today.vala:299
msgid "Language"
msgstr ""

#: src/Utils/OWM_Today.vala:275
msgid "Last Quarter Moon"
msgstr ""

#: src/Widgets/City.vala:61
msgid "Lat"
msgstr ""

#: src/Widgets/Preferences.vala:123
msgid "Launch on start"
msgstr ""

#: src/MainWindow.vala:74
msgid "Loading"
msgstr ""

#: src/Utils/OWM_Today.vala:175
msgid "Loading cache data"
msgstr ""

#: src/Utils/OWM_Today.vala:305 src/Widgets/City.vala:65
msgid "Location"
msgstr ""

#: src/Widgets/City.vala:62
msgid "Lon"
msgstr ""

#: src/Widgets/Header.vala:77
msgid "Maps"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:8
#: data/com.gitlab.bitseater.meteo.desktop.in:3
#: data/com.gitlab.bitseater.meteo.desktop.in:4
msgid "Meteo"
msgstr ""

#: data/com.gitlab.bitseater.meteo.desktop.in:13
msgid "Meteo;Weather;Forecast;Temperature;Wind;Snow;Rain;"
msgstr ""

#: src/Widgets/Preferences.vala:201
msgid "Metric System"
msgstr ""

#: src/Widgets/Today.vala:80
msgid "Moonphase"
msgstr ""

#: src/Utils/OWM_Today.vala:251
msgid "New Moon"
msgstr ""

#: src/Widgets/City.vala:131
msgid "No data"
msgstr ""

#: src/Utils/OWM_Today.vala:183
msgid "No data received, loading cache. Are you connected?"
msgstr ""

#: src/Widgets/Header.vala:51
msgid "Options"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:17
msgid "Other features:"
msgstr ""

#: src/Widgets/MapView.vala:100
msgid "Precipitation"
msgstr ""

#: src/Widgets/Preferences.vala:24 src/Widgets/Header.vala:35
msgid "Preferences"
msgstr ""

#: src/Utils/OWM_Today.vala:312 src/Widgets/MapView.vala:101
#: src/Widgets/Today.vala:54
msgid "Pressure"
msgstr ""

#: src/Utils/OWM_Today.vala:259
msgid "Quarter Moon"
msgstr ""

#: src/MainWindow.vala:189
msgid "Quit"
msgstr ""

#: src/Widgets/City.vala:49
msgid "Search for new location"
msgstr ""

#: src/MainWindow.vala:180
msgid "Show Meteo"
msgstr ""

#: src/Widgets/About.vala:58
msgid "Some icons by"
msgstr ""

#: src/Widgets/About.vala:56
msgid "Special Thanks to"
msgstr ""

#: src/Widgets/Preferences.vala:79
msgid "Start minimized"
msgstr ""

#: src/Utils/OWM_Today.vala:306 src/Widgets/City.vala:64
msgid "State"
msgstr ""

#: src/Utils/OWM_Today.vala:317 src/Widgets/Today.vala:72
msgid "Sunrise"
msgstr ""

#: src/Utils/OWM_Today.vala:318 src/Widgets/Today.vala:76
msgid "Sunset"
msgstr ""

#: src/Widgets/Preferences.vala:61
msgid "Symbolic icons"
msgstr ""

#: src/Utils/OWM_Today.vala:311 src/Widgets/MapView.vala:98
msgid "Temperature"
msgstr ""

#: src/Widgets/MapView.vala:106
msgid "Terms of Service"
msgstr ""

#: src/Widgets/Today.vala:36
msgid "Today"
msgstr ""

#: src/Widgets/Preferences.vala:203
msgid "UK System"
msgstr ""

#: src/Utils/OWM_Today.vala:301 src/Widgets/Preferences.vala:198
msgid "Units"
msgstr ""

#: src/Widgets/Header.vala:59
msgid "Update conditions"
msgstr ""

#: src/Widgets/Preferences.vala:143
msgid "Update conditions every"
msgstr ""

#: src/Widgets/Preferences.vala:97
msgid "Use System Tray Indicator"
msgstr ""

#: src/Utils/OWM_Today.vala:279
msgid "Waning Crescent Moon"
msgstr ""

#: src/Utils/OWM_Today.vala:271
msgid "Waning Gibbous Moon"
msgstr ""

#: data/com.gitlab.bitseater.meteo.appdata.xml.in:23
msgid "Watch awesome maps with weather information"
msgstr ""

#: src/Utils/OWM_Today.vala:255
msgid "Waxing Crescent Moon"
msgstr ""

#: src/Utils/OWM_Today.vala:263
msgid "Waxing Gibbous Moon"
msgstr ""

#: src/Utils/OWM_Today.vala:308
msgid "Weather"
msgstr ""

#: src/Widgets/MapView.vala:102
msgid "Wind Speed"
msgstr ""

#: src/Utils/OWM_Today.vala:315 src/Widgets/Today.vala:66
msgid "Wind dir"
msgstr ""

#: src/Utils/OWM_Today.vala:314 src/Widgets/Today.vala:62
msgid "Wind speed"
msgstr ""

#: src/Widgets/About.vala:49
msgid "website"
msgstr ""
