com.gitlab.bitseater.meteo (0.9.9.3) unstable; urgency=medium

    * Fixed some issues.
    * Add some languages.

 -- Carlos Suárez <bitseater@gmail.com>  Thu, 30 Jan 2024 00:00:00 +0100

com.gitlab.bitseater.meteo (0.9.9.2) unstable; urgency=medium

    * Change Maps provider
    * Fixed some issues.

 -- Carlos Suárez <bitseater@gmail.com>  Thu, 20 Jul 2023 00:00:00 +0100

com.gitlab.bitseater.meteo (0.9.9) unstable; urgency=medium

    * Delete geocode-glib dependencies.
    * Add Moon phases.
    * Add Temperature to tray popup.
    * Fixed issues: #132, #135, #136, #138.

 -- Carlos Suárez <bitseater@gmail.com>  Sat, 30 Oct 2021 13:20:00 +0100

com.gitlab.bitseater.meteo (0.9.8) unstable; urgency=medium

    * Indicator icon issue fixed.
    * Fixed some lintian warnings on packaging.
    * Fixed issues: #125, #128, #129.

 -- Carlos Suárez <bitseater@gmail.com>  Sat, 28 Sep 2019 22:58:00 +0100

com.gitlab.bitseater.meteo (0.9.7) unstable; urgency=medium

    * Changed method to search cities.
    * Added Dutch (.nl) language and update others languages.
    * Fixed some locale strings (Thanks Camellan).
    * Fixed issues: #118, #119, #120, #121.

 -- Carlos Suárez <bitseater@gmail.com>  Sun, 05 May 2019 12:00:00 +0100

com.gitlab.bitseater.meteo (0.9.6) unstable; urgency=medium

    * Some new improvement with units convertions
    * Added Norwegian Bokmål (.nb) translation.
    * Fixed issues: #116.

 -- Carlos Suárez <bitseater@gmail.com>  Mon, 11 Feb 2019 20:00:00 +0100

com.gitlab.bitseater.meteo (0.9.5) unstable; urgency=medium

    * New search city window.
    * No clutter dependencies.
    * More compatibility with many OS.
    * Fixed issues: 107, 108, 109, 111, 112, 114, 115.

 -- Carlos Suárez <bitseater@gmail.com>  Sat, 2 Feb 2019 20:00:00 +0100

com.gitlab.bitseater.meteo (0.8.5) unstable; urgency=medium

    * Add support to RPM build.
    * Fixed issues: none.

 -- Carlos Suárez <bitseater@gmail.com>  Tue, 07 Aug 2018 01:25:00 +0100

com.gitlab.bitseater.meteo (0.8.4) unstable; urgency=medium

    * Migration to GitLab.com.
    * Fixed issues: 98.

 -- Carlos Suárez <bitseater@gmail.com>  Wed, 13 Jun 2018 02:10:00 +0100

com.gitlab.bitseater.meteo (0.8.3) unstable; urgency=medium

    * Delete built-in API key which is blocked by OWM.
    * Fixed issues: 92, 96, 97.

 -- Carlos Suárez <bitseater@gmail.com>  Thu, 31 May 2018 10:00:00 +0100

com.gitlab.bitseater.meteo (0.8.2) unstable; urgency=medium

    * Delete built-in API key which is blocked by OWM.
    * Fixed issues: 92, 96.

 -- Carlos Suárez <bitseater@gmail.com>  Tue, 29 May 2018 20:40:00 +0100

com.gitlab.bitseater.meteo (0.8.1) unstable; urgency=medium

    * Simplify gettext strings.
    * Add new update interval.
    * Add more information to indicator.
    * Add new translations.
    * Show location on indicator menu.
    * Add run on startup option.
    * Change wind speed units.
    * Fixed issues: 72, 74, 80, 81, 82, 83, 84, 85, 86, 87, 88.

 -- Carlos Suárez <bitseater@gmail.com>  Mon, 06 May 2018 23:45:00 +0100

com.gitlab.bitseater.meteo (0.7.0) unstable; urgency=medium

    * Load data from cache file when no connect.
    * Add some comments when user select built-in API key.
    * Add more information to indicator.
    * Add new translations.
    * Fixed issues: 49,50,52,53,57,58,60,61.

 -- Carlos Suárez <bitseater@gmail.com>  Thu, 22 Mar 2018 21:30:00 +0100

com.gitlab.bitseater.meteo (0.6.1) unstable; urgency=medium

    * Change appname to METEO.
    * Save currenta data to cache file.
    * Show cache file in text format.
    * Add new translations: pt_PT and pt_BR.
    * Reload button when own API key.

 -- Carlos Suárez <bitseater@gmail.com>  Wed, 21 Feb 2018 01:30:00 +0100

com.gitlab.bitseater.meteo (0.5.3) unstable; urgency=medium

    * Change to meson build.
    * Add new translations.
    * System tray indicator with weather icons.
    * Weather Maps for temperature, clouds, rain, pressure and wind speed.

 -- Carlos Suárez <bitseater@gmail.com>  Sat, 10 Feb 2018 19:50:00 +0100

com.gitlab.bitseater.meteo (0.4.1) unstable; urgency=medium

  * Fix application icon issue.

 -- Carlos Suárez <bitseater@gmail.com>  Sat, 09 Dec 2017 04:45:00 +0100

com.gitlab.bitseater.meteo (0.4) unstable; urgency=medium

  * Add cmake support to other SO than elementaryOS.
  * Add lituanian and russian translations. Thx to welaq and camellan.
  * Fix gettext issue. Thx to Andrey Kultyapov <camellan@yandex.ru>.
  * Add support to Debian/Ubuntu.
  * Fix visual issues in UI.
  * CSS support added.
  * New "find location" screen, with elegants maps.
  * Automatic location on start (Can disabled on preferences).
  * Choose between realistic and symbolic icons.

 -- Carlos Suárez <bitseater@gmail.com>  Fri, 08 Dec 2017 15:10:00 +0100

com.gitlab.bitseater.meteo (0.3) unstable; urgency=medium

  * Fix some issues and support map layers on city selection.

 -- Carlos Suárez <bitseater@gmail.com>  Sun, 26 Nov 2017 21:27:00 +0100

com.gitlab.bitseater.meteo (0.2) unstable; urgency=medium

  * Adding tooltips and fixing translations issues.

 -- Carlos Suárez <bitseater@gmail.com>  Fri, 18 Nov 2017 20:43:00 +0100

com.gitlab.bitseater.meteo (0.1) unstable; urgency=low

  * Initial Release.

 -- Carlos Suárez <bitseater@gmail.com>  Fri, 17 Nov 2017 22:00:00 +0100
