/*
* Copyright (c) 2017-2018 Carlos Suárez (https://gitlab.com/bitseater)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; If not, see <http://www.gnu.org/licenses/>.
*
* Authored by: Carlos Suárez <bitseater@gmail.com>
*/
namespace Meteo.Widgets {

    public class MapView : Gtk.Box {

		public MapView (Meteo.MainWindow window, Meteo.Widgets.Header header) {
            orientation = Gtk.Orientation.VERTICAL;

            //Define latitude y longitude and units
            string lat = "";
            string lon = "";
            var setting = new Settings ("com.gitlab.bitseater.meteo");
            string apiid = setting.get_string ("apiid");
            string idplace = setting.get_string ("idplace");
            string uri = Constants.OWM_API_ADDR + "weather?id=" + idplace + "&APPID=" + apiid;

            var session = new Soup.Session ();
            var message = new Soup.Message ("GET", uri);
            session.send_message (message);

            try {
                var parser = new Json.Parser ();
                parser.load_from_data ((string) message.response_body.flatten ().data, -1);

                var root = parser.get_root ().get_object ();
                var coord = root.get_object_member ("coord");
                lat = Meteo.Utils.to_string2 (coord.get_double_member ("lat"));
                lon = Meteo.Utils.to_string2 (coord.get_double_member ("lon"));
            } catch (Error e) {
                stdout.printf (_("Found an error") + ": %s\n", e.message);
            }

            //Define maps URL's
            string url_serv = "https://map.worldweatheronline.com/temperature?lat=";
            string url_ok = url_serv + lat + "&lng=" + lon;

            //Define other elements
            var prov_lab = new Gtk.Label ("\xc2\xa9 WorldWeatherOnline");
            var tos = new Gtk.LinkButton.with_label ("https://map.worldweatheronline.com", _("Link to website"));
            tos.halign = Gtk.Align.END;

            //Pack combo to actionbar
            var prov_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 3);
            prov_box.margin = 3;
            prov_box.pack_start (prov_lab , false, false, 3);
            prov_box.pack_end (tos, false, false, 3);
            var action_box = new Gtk.ActionBar ();
            action_box.get_style_context ().add_class (Gtk.STYLE_CLASS_INLINE_TOOLBAR);
            action_box.pack_start (prov_box);

            //Pack elementes
            pack_start (new Meteo.Utils.MapLayer (url_ok), true, true, 0);
            pack_end (action_box, false, false, 0);
        }
    }
}
